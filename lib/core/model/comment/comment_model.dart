class CommentModel {
  final List<dynamic> record;

  CommentModel({this.record});

  factory CommentModel.fromJson(List<dynamic> json) {
    return CommentModel(
      record: json != null ? new List<dynamic>.from(json) : null,
    );
  }

  List<dynamic> toJson() {
    List<dynamic> data = [];
    if (this.record != null) {
      data = this.record;
    }
    return data;
  }
}