class PostModel {
  final List<dynamic> record;

  PostModel({this.record});

  factory PostModel.fromJson(List<dynamic> json) {
    return PostModel(
      record: json != null ? new List<dynamic>.from(json) : null,
    );
  }

  List<dynamic> toJson() {
    List<dynamic> data = [];
    if (this.record != null) {
      data = this.record;
    }
    return data;
  }
}