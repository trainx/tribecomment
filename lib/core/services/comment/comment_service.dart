import 'dart:async';

import 'package:tribecomment/core/model/comment/comment_model.dart';
import 'package:tribecomment/core/provider/api_sync.dart';

class CommentService {
  ApiSync _provider = ApiSync();

  Future<CommentModel> fetchCommentDataByPostId({int id}) async {
    final response = await _provider.getJsonRecord("comments?postId=" + id.toString());
    return CommentModel.fromJson(response);
  }
}