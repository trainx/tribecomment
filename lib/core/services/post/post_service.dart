import 'dart:async';

import 'package:tribecomment/core/model/post/post_model.dart';
import 'package:tribecomment/core/provider/api_sync.dart';

class PostService {
  ApiSync _provider = ApiSync();

  Future<PostModel> fetchPostData() async {
    final response = await _provider.getJsonRecord("posts");
    return PostModel.fromJson(response);
  }

  Future<PostModel> fetchPostDataById({int id}) async {
    final response = await _provider.getJsonRecord("posts/" + id.toString());
    return PostModel.fromJson([response]);
  }
}