import 'package:tribecomment/shared/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tribecomment/shared/routes.dart' as router;

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent, // transparent status bar
  ));
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primaryColor: mainColor,
        textTheme: GoogleFonts.poppinsTextTheme(
          textTheme,
        ),
        appBarTheme: AppBarTheme(
          textTheme: GoogleFonts.poppinsTextTheme(textTheme).copyWith(
            headline6: GoogleFonts.poppins(color: Colors.white, fontSize: 18)
          ),
          brightness: Brightness.dark,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
      ),
      onGenerateRoute: router.generateRoute,
      initialRoute: SPLASH
    );
  }
}