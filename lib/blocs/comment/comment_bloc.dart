import 'dart:async';
import 'package:tribecomment/core/model/comment/comment_model.dart';
import 'package:tribecomment/core/provider/response.dart';
import 'package:tribecomment/core/services/comment/comment_service.dart';

class CommentBloc {
  CommentService _commentService;
  StreamController _resultController;
  List<dynamic> commentList;

  StreamSink<Response<CommentModel>> get resultSink => _resultController.sink;
  Stream<Response<CommentModel>> get resultStream => _resultController.stream;

  CommentBloc() {
    _resultController = StreamController<Response<CommentModel>>();
    // _resultController = new BehaviorSubject<Response<CommentModel>>();
    _commentService = CommentService();
  }

  fetchComments(int id) async {
    resultSink.add(Response.loading('Getting record...'));
    try {
      CommentModel record = await _commentService.fetchCommentDataByPostId(id: id);
      commentList = record.record.toList();
      resultSink.add(Response.completed(record));
    } catch (e) {
      resultSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  searchNewsList(String query) {
    if (query != null) {
      CommentModel record = CommentModel(record: (commentList.where((a) {
        return a['name'].toLowerCase().contains(query.toLowerCase().trim()) || a['email'].toLowerCase().contains(query.toLowerCase().trim())
        || a['body'].toLowerCase().contains(query.toLowerCase().trim());
      }).toList()));
      resultSink.add(Response.completed(record));
    }
  }

  dispose() {
    _resultController?.close();
  }
}