import 'dart:async';
import 'package:tribecomment/core/model/post/post_model.dart';
import 'package:tribecomment/core/provider/response.dart';
import 'package:tribecomment/core/services/post/post_service.dart';

class PostBloc {
  PostService _postService;
  StreamController _resultController;

  StreamSink<Response<PostModel>> get resultSink => _resultController.sink;
  Stream<Response<PostModel>> get resultStream => _resultController.stream;

  PostBloc() {
    _resultController = StreamController<Response<PostModel>>();
    // _resultController = new BehaviorSubject<Response<PostModel>>();
    _postService = PostService();
  }

  fetchPosts() async {
    resultSink.add(Response.loading('Getting record...'));
    try {
      PostModel record = await _postService.fetchPostData();
      resultSink.add(Response.completed(record));
    } catch (e) {
      resultSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  fetchPostById(int id) async {
    resultSink.add(Response.loading('Getting record...'));
    try {
      PostModel record = await _postService.fetchPostDataById(id: id);
      resultSink.add(Response.completed(record));
    } catch (e) {
      resultSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _resultController?.close();
  }
}