import 'package:tribecomment/shared/constant.dart';
import 'package:tribecomment/views/post/post.dart';
import 'package:tribecomment/views/splash/splash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  return MaterialPageRoute(
    settings: settings,
    builder: (BuildContext context) {
      switch (settings.name) {
        case SPLASH:
          return SplashState();
        case HOME:
          return const PostState();
        default: 
          return Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}')
            ),
          );
      }
    }
  );
}