import 'package:flutter/material.dart';
import 'package:tribecomment/shared/constant.dart';
import 'package:tribecomment/shared/utils.dart';

class ErrorSync extends StatelessWidget {
  final String errorMessage;

  final Function onRetryPressed;

  const ErrorSync({Key key, this.errorMessage, this.onRetryPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            errorMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontSize: 18,
            ),
          ),
          SizedBox(height: 8),
          ElevatedButton(
            onPressed: onRetryPressed,
            style: ButtonStyle(
              overlayColor: MaterialStateProperty.resolveWith(
                (states) {
                  return states.contains(MaterialState.pressed)
                    ? Colors.red[900]
                    : null;
                },
              ),
              elevation: MaterialStateProperty.all<double>(1.0),
              backgroundColor: MaterialStateProperty.all<Color>(mainColor),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))
                )
              )
            ),
            child: Text(
              "Retry",
              style: utils.getTextStyleRegular(fontSize: 14, color: Colors.white)
            ),
          )
        ],
      ),
    );
  }
}