import 'package:flutter/material.dart';

const Color mainColor = const Color(0xFF780206);

const API_URL = "https://jsonplaceholder.typicode.com/";
const String SPLASH = '/';
const String HOME = '/home';