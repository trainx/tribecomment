import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Utils {
  factory Utils() {
    return _singleton;
  }

  static final Utils _singleton = Utils._internal();
  Utils._internal() {
    // print("Instance created Utils");
  }

  //Region Screen Size and Proportional according to device
  double _screenHeight;
  double _screenWidth;
  
  double get screenHeight => _screenHeight;
  double get screenWidth => _screenWidth;

  final double _refrenceScreenHeight = 640;
  final double _refrenceScreenWidth = 360;

  void updateScreenDimesion({double width, double height}) {
    _screenWidth = (width != null) ? width : _screenWidth;
    _screenHeight = (height != null) ? height : _screenHeight;
  }

  double getProportionalHeight({double height}) {
    if (_screenHeight == null) return height;
    return _screenHeight * height / _refrenceScreenHeight;
  }

  double getProportionalWidth({double width}) {
    if (_screenWidth == null) return width;
    var w = _screenWidth * width / _refrenceScreenWidth;
    return w.ceilToDouble();
  }

  double getButtonHeight({double height}) {
    double h = height / _refrenceScreenHeight * 40;
    return h;
  }

  //Region TextStyle
  TextStyle getTextStyleRegular(
    {
      String fontName,
      int fontSize = 12,
      Color color = Colors.black,
      bool isChangeAccordingToDeviceSize = true,
      double characterSpacing,
      double lineSpacing,
      FontWeight weight = FontWeight.normal,
      TextDecoration underline = TextDecoration.none,
      FontStyle fontStyle,
      Color bgColor,
      double height,
    }
  ) 
  {
    double finalFontsize = fontSize.toDouble();
    if (isChangeAccordingToDeviceSize && this._screenWidth != null) {
      finalFontsize = (finalFontsize * _screenWidth) / _refrenceScreenWidth;
    }

    // print("Font size " + finalFontsize.toStringAsFixed(2));
    if (characterSpacing != null) {
      return GoogleFonts.poppins(
        // fontSize: finalFontsize,
        // fontFamily: fontName,
        color: color,
        letterSpacing: characterSpacing,
        fontWeight: weight,
        decoration: underline,
        fontStyle: fontStyle,
        backgroundColor: bgColor
      );
    } else if (lineSpacing != null) {
      return GoogleFonts.poppins(
        // fontSize: finalFontsize,
        // fontFamily: fontName,
        color: color,
        height: lineSpacing,
        fontWeight: weight,
        decoration: underline,
        fontStyle: fontStyle,
        backgroundColor: bgColor
      );
    }
    return GoogleFonts.poppins(fontSize: finalFontsize, color: color, fontWeight: weight, decoration: underline, fontStyle: fontStyle, backgroundColor: bgColor, height: height);
  }

  Widget backHeaderButton(BuildContext context, {String type, IconData backIcon, VoidCallback onClick}) {
    String typeMode = type != null ? type : 'back';
    return Padding(
      padding: EdgeInsets.all(10),
      child: Material(
        shape: StadiumBorder(),
        color: Colors.black87.withOpacity(0.5),
        child: InkWell(
          customBorder: new CircleBorder(),
          child: ClipOval(
            child: Icon(typeMode == 'back' ? backIcon ?? Icons.arrow_back_ios_outlined : Icons.close, size: 20, color: Colors.white)
          ),
          onTap: () {
            if (onClick == null) {
              Navigator.pop(context, true);
            } else {
              onClick();
              Navigator.pop(context, true);
            }
          }
        )
      )
    );
  }

  Widget cirlceRippleButton(BuildContext context, {@required Icon icon, @required VoidCallback onClick, Color color, Color borderColor}) {
    return Container(
      height: icon.size != null ? 20 + icon.size : 45,
      width: icon.size != null ? 20 + icon.size : 45,
      // padding: EdgeInsets.all(7),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        border: Border.all(width: 2.0, color: borderColor ?? Colors.transparent)
      ),
      child: Material(
        shape: StadiumBorder(),
        color: color ?? Colors.grey[200],
        child: InkWell(
          customBorder: new CircleBorder(),
          child: ClipOval(
            child: icon
          ),
          onTap: onClick
        )
      )
    );
  }
}

final utils = Utils();