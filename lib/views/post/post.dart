import 'package:tribecomment/blocs/post/post_bloc.dart';
import 'package:tribecomment/core/model/post/post_model.dart';
import 'package:tribecomment/core/provider/response.dart';
import 'package:tribecomment/shared/constant.dart';
import 'package:tribecomment/shared/error_sync.dart';
import 'package:tribecomment/shared/utils.dart';
import 'package:tribecomment/views/post/post_details.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class PostState extends StatefulWidget {
  const PostState({Key key}) : super(key: key);
  
  @override
  _PostState createState() => _PostState();
}

class _PostState extends State<PostState> {
  final ScrollController scrollController = new ScrollController();
  double _offSet = 0.0;

  PostBloc _bloc;
  List<dynamic> records = [];

  @override
  void initState() {
    super.initState();

    _bloc = new PostBloc();
    _bloc.fetchPosts();

    scrollController.addListener(() => 
      setState(() {
        //<-----------------------------
        _offSet = scrollController.offset;
        // print('_offSet $_offSet');
        // force a refresh so the app bar can be updated
      })
    );
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        // For Android.
        // Use [light] for white status bar and [dark] for black status bar.
        statusBarIconBrightness: Brightness.dark,
        // For iOS.
        // Use [dark] for white status bar and [light] for black status bar.
        statusBarBrightness: Brightness.light,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          brightness: Brightness.light,
          elevation: _offSet > 6 ? 1.0 : 0.0,
          title: Text(
            'Posts',
            style: utils.getTextStyleRegular(color: mainColor, fontSize: 22, weight: FontWeight.bold)
          ),
          backgroundColor: Colors.white
        ),
        body: StreamBuilder<Response<PostModel>>(
          stream: _bloc.resultStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      // enabled: _enabled,
                      child: ListView.builder(
                        itemBuilder: (_, __) => Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 48.0,
                                height: 48.0,
                                decoration: BoxDecoration(
                                  color: Colors.orange,
                                  shape: BoxShape.circle
                                )
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 12.0),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(vertical: 6.0),
                                    ),
                                    Container(
                                      width: 40.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        itemCount: 1,
                      ),
                    )
                  );
                  break;
                case Status.COMPLETED:
                  records = snapshot.data.data.record;
                  return records.length != 0 ? Container(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: records.length,
                      itemBuilder: (context, index) => Column(
                        children: <Widget>[
                          _postsList(context, records[index])
                        ]
                      )
                    )
                  ) : Center(child: Text('No record found'));
                  break;
                case Status.ERROR:
                  return ErrorSync(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => _bloc.fetchPosts(),
                  );
                  break;
              }
            }
            return Container();
          },
        )
      )
    );
  }

  Widget _postsList(BuildContext context, item) {
    int _postId = item['id'] != null ? item['id'] : '-';
    int _userId = item['userId'] != null ? item['userId'] : '-';
    String _title = item['title'] != null ? item['title'] : '-';

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      margin: EdgeInsets.fromLTRB(2, 5, 2, 5),
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey[300]),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 40,
                      padding: EdgeInsets.only(left: 15),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        border: Border.all(color: Colors.grey[300]),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(Icons.account_circle, size: 20, color: Colors.black),
                              SizedBox(width: 5),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Container(
                                  width: 300,
                                  child: Text(
                                    _userId.toString(),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: utils.getTextStyleRegular(color: Colors.black, fontSize: 15),
                                  ),
                                ),
                              )
                            ]
                          ),
                        ]
                      )
                    ),
                    SizedBox(height: 10),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: RichText(
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(text: 'TITLE:  ', style: utils.getTextStyleRegular(color: Colors.black, fontSize: 12, weight: FontWeight.bold)),
                            TextSpan(text: _title, style: utils.getTextStyleRegular(color: Colors.black, fontSize: 12))
                          ]
                        ),
                      ),
                    ),
                    SizedBox(height: 10)
                  ]
                )
              )
            ]
          ),
          Positioned.fill(
            child: new Material(
              color: Colors.transparent,
              child: new InkWell(
                onTap: () {
                  Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => PostDetailState(postId: _postId)));
                }
              )
            )
          )
        ]
      ),
    );
  }
}