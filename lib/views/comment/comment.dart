import 'package:tribecomment/blocs/comment/comment_bloc.dart';
import 'package:tribecomment/core/model/comment/comment_model.dart';
import 'package:tribecomment/core/provider/response.dart';
import 'package:tribecomment/shared/constant.dart';
import 'package:tribecomment/shared/error_sync.dart';
import 'package:tribecomment/shared/utils.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CommentState extends StatefulWidget {
  final int postId;
  const CommentState({Key key, @required this.postId}) : super(key: key);
  
  @override
  _CommentState createState() => _CommentState();
}

class _CommentState extends State<CommentState> {
  final TextEditingController searchController = TextEditingController();

  bool _isSearching = false;
  CommentBloc _bloc;
  List<dynamic> records = [];

  @override
  void initState() {
    super.initState();

    _bloc = new CommentBloc();
    _bloc.fetchComments(widget.postId);

    searchController.addListener(() {
      setState(() {
        searchController.text;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        // For Android.
        // Use [light] for white status bar and [dark] for black status bar.
        statusBarIconBrightness: Brightness.dark,
        // For iOS.
        // Use [dark] for white status bar and [light] for black status bar.
        statusBarBrightness: Brightness.light,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          leading: utils.backHeaderButton(context, backIcon: _isSearching ? Icons.close : Icons.arrow_back_ios_outlined, onClick: _isSearching ? _clearSearchQuery : null),
          brightness: Brightness.light,
          elevation: 0.0,
          title: _buildSearch(_isSearching),
          backgroundColor: Colors.white,
          actions: [
            Container(
              padding: EdgeInsets.only(right: 10),
              child: _buildAction(_isSearching)
            )
          ]
        ),
        body: StreamBuilder<Response<CommentModel>>(
          stream: _bloc.resultStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      // enabled: _enabled,
                      child: ListView.builder(
                        itemBuilder: (_, __) => Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 48.0,
                                height: 48.0,
                                decoration: BoxDecoration(
                                  color: Colors.orange,
                                  shape: BoxShape.circle
                                )
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 12.0),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(vertical: 6.0),
                                    ),
                                    Container(
                                      width: 40.0,
                                      height: 8.0,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        itemCount: 1,
                      ),
                    )
                  );
                  break;
                case Status.COMPLETED:
                  records = snapshot.data.data.record;
                  return records.length != 0 ? Container(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: records.length,
                      itemBuilder: (context, index) => Column(
                        children: <Widget>[
                          _commentList(context, records[index])
                        ]
                      )
                    )
                  ) : Center(child: Text('No record found'));
                  break;
                case Status.ERROR:
                  return ErrorSync(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => _bloc.fetchComments(widget.postId),
                  );
                  break;
              }
            }
            return Container();
          },
        )
      )
    );
  }

  Widget _commentList(BuildContext context, item) {
    // int _postId = item['postId'] != null ? item['postId'] : '-';
    int _id = item['id'] != null ? item['id'] : '-';
    String _name = item['name'] != null ? item['name'] : '-';
    String _email = item['email'] != null ? item['email'] : '-';
    String _body = item['body'] != null ? item['body'] : '-';

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      margin: EdgeInsets.fromLTRB(2, 5, 2, 5),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey[300]),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  height: 40,
                  padding: EdgeInsets.only(left: 15),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    border: Border.all(color: Colors.grey[300]),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(Icons.account_circle, size: 20, color: Colors.black),
                          SizedBox(width: 5),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              width: 300,
                              child: Text(
                                _id.toString(),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: utils.getTextStyleRegular(color: Colors.black, fontSize: 15),
                              ),
                            ),
                          )
                        ]
                      ),
                    ]
                  )
                ),
                SizedBox(height: 10),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(text: 'NAME:  ', style: utils.getTextStyleRegular(color: Colors.black, fontSize: 12, weight: FontWeight.bold)),
                            TextSpan(text: _name, style: utils.getTextStyleRegular(color: Colors.black, fontSize: 12))
                          ]
                        ),
                      ),
                      RichText(
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(text: 'EMAIL:  ', style: utils.getTextStyleRegular(color: Colors.black, fontSize: 12, weight: FontWeight.bold)),
                            TextSpan(text: _email, style: utils.getTextStyleRegular(color: Colors.black, fontSize: 12))
                          ]
                        ),
                      ),
                      RichText(
                        overflow: TextOverflow.ellipsis,
                        maxLines: 4,
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(text: 'BODY:  ', style: utils.getTextStyleRegular(color: Colors.black, fontSize: 12, weight: FontWeight.bold)),
                            TextSpan(text: _body, style: utils.getTextStyleRegular(color: Colors.black, fontSize: 12))
                          ]
                        ),
                      )
                    ]
                  )
                ),
                SizedBox(height: 10)
              ]
            )
          )
        ]
      )
    );
  }

  Widget _buildSearch(bool hasSearch) {
    return !hasSearch ? Text(
      'Comment',
      style: utils.getTextStyleRegular(color: mainColor, fontSize: 22, weight: FontWeight.bold)
    ) : TextField(
      controller: searchController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: 'Search...',
        border: InputBorder.none,
        hintStyle: utils.getTextStyleRegular(fontSize: 13, color: Colors.grey),
      ),
      style: utils.getTextStyleRegular(color: Colors.black, fontSize: 14),
      onChanged: _bloc.searchNewsList,
    );
  }

  Widget _buildAction(bool hasSearch) {
    return !hasSearch ? utils.cirlceRippleButton(
      context,
      icon: Icon(Icons.search_rounded, size: 25, color: Colors.redAccent),
      color: Colors.transparent,
      onClick: () {
        if (records.length != 0) {
          _startSearching();
        }
      }
    ) : (searchController.text != '' ? utils.cirlceRippleButton(
      context,
      icon: Icon(Icons.delete_forever, size: 25, color: Colors.redAccent),
      color: Colors.transparent,
      onClick: () {
        if (searchController == null || searchController.text.isEmpty) {
          Navigator.pop(context);
          return;
        }
        _clearSearchQuery();
      }
    ) : Container());
  }

  void _startSearching() {
    ModalRoute.of(context).addLocalHistoryEntry(new LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  void _stopSearching() {
    setState(() {
      _isSearching = false;
    });
  }

  void _clearSearchQuery() {
    setState(() {
      searchController.clear();
      _bloc.searchNewsList('');
    });
  }
}